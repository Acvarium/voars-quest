extends Spatial

func button_pressed(hand_id, button_id):
	if hand_id == 1 and button_id == 2:
		$MotorBoat.enable_motor(true)

func button_release(hand_id, button_id):
	if hand_id == 1 and button_id == 2:
		$MotorBoat.enable_motor(false)

func _physics_process(delta):
	var control_basis = $MotorBoat/ARVROrigin/RightTouchController.global_transform.basis
	control_basis = control_basis.rotated(-Basis().y, PI/2.0)
	$MotorBoat.set_dir(control_basis)
