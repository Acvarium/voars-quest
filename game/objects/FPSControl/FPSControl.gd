extends KinematicBody
#--Basic FPS character control
#------------------------------
export var lock_y = false
onready var main_node = get_tree().get_root().get_node("main")
var camera_angle = 0
var mouse_sens = 0.3
var velocity = Vector3()
var direction = Vector3()
var camera_change = Vector2()

const FLY_SPEED = 4
const FLY_ACCEL = 1
var flyung = false
var mause_locked = false

var gravity = -9.8 * 3

const MAX_SPEED = 5
const MAX_RUN_SPEED = 10
const ACCEL = 2
const DEACCEL = 6

var jump_hight = 10
var has_contact = false

const MAX_SLOPE_ANGLE = 35

func mouse_lock(to_lock):
	mause_locked = to_lock
	if mause_locked:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _ready():
	main_node = get_tree().get_root().get_node("main")
	mouse_lock(true)

func _input(event):
	if event is InputEventMouseMotion:
		camera_change = event.relative
	if event.is_action_pressed("ui_cancel"):
		mouse_lock(!mause_locked)
		
func _physics_process(delta):
	if mause_locked:
		aim()
	if flyung:
		fly(delta)
	else:
		walk(delta)

func walk(delta):
	direction = Vector3()
	var aim = $head/Camera.global_transform.basis
	if Input.is_action_pressed("ui_up"):
		direction -= aim.z
	if Input.is_action_pressed("ui_down"):
		direction += aim.z
	if Input.is_action_pressed("ui_left"):
		direction -= aim.x
	if Input.is_action_pressed("ui_right"):
		direction += aim.x
	direction.y = 0
	
	direction = direction.normalized()
	if is_on_floor():
		has_contact = true
		var n = $tail.get_collision_normal()
		var floor_angle = rad2deg(acos(n.dot(Vector3(0,1,0))))
		if floor_angle > MAX_SLOPE_ANGLE:
			velocity.y += gravity * delta
	else:
		if !$tail.is_colliding():
			has_contact = false
		velocity.y += gravity * delta
	if lock_y:
		velocity.y = 0.0
		
	if has_contact and !is_on_floor():
		move_and_collide(Vector3(0,-1,0))
	
#	velocity.y += gravity * delta
	
	var temp_velocity = velocity
	temp_velocity.y = 0
	
	var speed 
	if Input.is_action_pressed("sprint") and has_contact:
		speed = MAX_RUN_SPEED
	else:
		speed = MAX_SPEED
		
	
	var target = direction * speed
	
	var acceleration 
	if direction.dot(temp_velocity) > 0:
		acceleration = ACCEL
	else:
		acceleration = DEACCEL
		
	
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
	
	velocity.x = temp_velocity.x
	velocity.z = temp_velocity.z
	
	if has_contact and Input.is_action_just_pressed("jump"):
		velocity.y = jump_hight
		has_contact = false
	
	
	velocity = move_and_slide(velocity, Vector3(0,1,0))

func fly(delta):
	direction = Vector3()
	var aim = $head/Camera.global_transform.basis
	if Input.is_action_pressed("ui_up"):
		direction -= aim.z
	if Input.is_action_pressed("ui_down"):
		direction += aim.z
	if Input.is_action_pressed("ui_left"):
		direction -= aim.x
	if Input.is_action_pressed("ui_right"):
		direction += aim.x
		
	direction = direction.normalized()
	
	var target = direction * FLY_SPEED
	
	velocity = velocity.linear_interpolate(target, FLY_ACCEL * delta)
	
	move_and_slide(velocity)
	
func aim():
	if camera_change.length() > 0:
		$head.rotate_y(deg2rad(-camera_change.x * mouse_sens))
		var change = -camera_change.y * mouse_sens
		if change + camera_angle < 90 and change + camera_angle > -90:
			$head/Camera.rotate_x(deg2rad(change))
			camera_angle += change
		camera_change = Vector2()


func _on_Area_body_entered(body):
	if body.name == "John":
		flyung = true

func _on_Area_body_exited(body):
	if body.name == "John":
		flyung = false
