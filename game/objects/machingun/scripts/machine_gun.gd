extends Spatial
onready var main_node = get_tree().get_root().get_node("main")
var hRotLimit = PI/180.0 * 30.0 
var vRotLimit = PI/180.0 * 13.0
const muzzle_offset = 0.542
var m_heat = 0.0
const heat_speed = 2.0

onready var vControlID = $machin_gun_arm/Skeleton.find_bone("v")#$mGun_base/mGun_ring
onready var hControlID = $machin_gun_arm/Skeleton.find_bone("h")#$mGun_base/mGun_ring/mGun
#onready var muzzle = $mGun_base/mGun_ring/mGun/mGun_muzzle

var target : Spatial
var hhRot = 0.0
var vvRot = 0.0
var animSpeed = 0.0
var shooting = false
var turn_speed = 0.35
var to_shoot = false
var b_rot_speed = 0.0
var b_rot_acc = 6.0
var tar_dot = 0.0
var over_heat = false

func _ready():
	$AnimationPlayer.get_animation("ch0").loop = true
	$AnimationPlayer.get_animation("shooting").loop = true
	$AnimationPlayer.get_animation("idle").loop = true
	
func _physics_process(delta):
	var heat_inc = -heat_speed * delta
	if shooting:
		heat_inc = heat_speed * delta
	m_heat += heat_inc
	if m_heat > 10.0:
		stop_shooting()
		over_heat = true
		$machin_gun_arm/Skeleton/hAttachment/hissSound.play()
	m_heat = clamp(m_heat, 0.0, 10.0)
	var shoot_speed_inc = -b_rot_acc
	if (shooting):
		shoot_speed_inc = b_rot_acc
	
	if over_heat and m_heat < 5.0:
		over_heat = false
		
	b_rot_speed += delta * shoot_speed_inc
	if b_rot_speed > 2.0:
		b_rot_speed = 2.0
	elif b_rot_speed < 0.0:
		b_rot_speed = 0.0
	
	if b_rot_speed > 0.2:
		$machin_gun_arm/Skeleton/fire.visible = true
		if !$machin_gun_arm/Skeleton/hAttachment/fireSound.playing:
			$machin_gun_arm/Skeleton/hAttachment/fireSound.play()
		var sound_speed = clamp(b_rot_speed, 0.1, 0.8) + 0.2
		$machin_gun_arm/Skeleton/hAttachment/fireSound.pitch_scale = sound_speed			
	else:
		if $machin_gun_arm/Skeleton/hAttachment/fireSound.playing:
			$machin_gun_arm/Skeleton/hAttachment/fireSound.stop()
			$machin_gun_arm/Skeleton/fire.visible = false
			
	$AnimationTree.set("parameters/shootSpeed/scale", b_rot_speed)
#	if $machin_gun_arm/Skeleton/hAttachment/RayCast.is_colliding():
#		$ball.visible = true
#		$ball.global_transform.origin = \
#			$machin_gun_arm/Skeleton/hAttachment/RayCast.get_collision_point()
#	else:
#		$ball.visible = false
		
	if Input.is_action_just_pressed("fire"):
		$AnimationTree.set("parameters/OneShot/active", true)
#	muzzle.transform.basis = muzzle.transform.basis.rotated(
#		Vector3.FORWARD, 5.0 * delta)
	if target != null:
		var targetPoint = target.global_transform.origin
		targetPoint.y = targetPoint.y - muzzle_offset

		var tDot = (-$machin_gun_arm/Skeleton/hAttachment/RayCast.global_transform.basis.z.normalized()).dot(
			(targetPoint - global_transform.origin).normalized()
		)
		if (tar_dot < 0.7):
			targetPoint = $targetPoint.global_transform.origin
			
		var vControl = $machin_gun_arm/Skeleton.get_bone_pose(vControlID)
		
		var basePoint = $basePoint.global_transform.origin #vControl.global_transform.origin
		var hTarget = targetPoint
		hTarget.y = basePoint.y
		var vVec = basePoint - targetPoint
		var vRot = (basePoint - hTarget).angle_to(vVec) * sign(
			basePoint.y - targetPoint.y)
		var zeroVec = Vector3(1.0, 0.0, 1.0)
		var hVec = (hTarget * zeroVec) - (basePoint * zeroVec)
		var hRot = (-global_transform.basis.z).angle_to(hVec) * (sign(
			(-global_transform.basis.x).normalized().dot(hVec.normalized())))
		vRot = clamp(vRot, -vRotLimit, vRotLimit)
		hRot = clamp(hRot, -hRotLimit, hRotLimit)
# warning-ignore:unused_variable
		var speedMult = 1
		if (abs(vvRot - vRot) < 0.01):
			vvRot = vRot
			animSpeed -= delta
			if animSpeed < 0:
				animSpeed = 0
		else:
			animSpeed = 1.0 
			vvRot += turn_speed * sign(vRot - vvRot) * delta
			speedMult = sign(vRot - vvRot)
			
		if (abs(hhRot - hRot) < 0.01):
			hhRot = hRot
			animSpeed -= delta
			if animSpeed < 0:
				animSpeed = 0
		else:
			animSpeed = 1.0 
			hhRot += turn_speed * sign(hRot - hhRot) * delta
			speedMult = sign(hRot - hhRot)
			
		vControl.basis = Basis().rotated(Vector3.LEFT, vvRot)
		$machin_gun_arm/Skeleton.set_bone_pose(
			vControlID, vControl)
		
		$machin_gun_arm/Skeleton.set_bone_pose(
			hControlID, Basis().rotated(Vector3.UP, hhRot))
		$AnimationTree.set("parameters/TimeScale/scale", animSpeed * 2.0)
		if (animSpeed < 0.1):
			if ($pedalSound.is_playing()):
				$pedalSound.stop()
		elif (!$pedalSound.is_playing()):
			$pedalSound.play()
		
		$pedalSound.pitch_scale = clamp(animSpeed, 0.1, 3.0)
		tar_dot = (tar_dot + tDot) / 2.0
		
		if (tar_dot > 0.99 and !shooting):
			start_shooting()
	else:
		if main_node.has_node("debug_control"):
			if main_node.get_node("debug_control").target_obj != null:
				target = main_node.get_node("debug_control").target_obj
				
	$AnimationTree.set("parameters/Seek/seek_position", m_heat)
	
func start_shooting():
	if over_heat:
		return
	if to_shoot:
		return
	if b_rot_speed < 0.1:
		$AnimationTree.set("parameters/OneShot/active", true)
		$machin_gun_arm/Skeleton/hAttachment/buttonSound.play()
	$shootStartDelay.start()
	to_shoot = true

func stop_shooting():
	shooting = false

func _on_shootingTimer_timeout():
	if target == null:
		stop_shooting()
	else:
		var tDot = (-$machin_gun_arm/Skeleton/hAttachment/RayCast.global_transform.basis.z.normalized()).dot(
			(target.global_transform.origin - global_transform.origin).normalized())
		if (tDot < 0.96):
			stop_shooting()
		else:
			$shootingTimer.start()
func _on_shootStartDelay_timeout():
	shooting = true
	to_shoot = false
	$shootingTimer.start()
	
	
