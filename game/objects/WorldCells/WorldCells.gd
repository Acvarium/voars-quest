extends Spatial

onready var main_node = get_tree().get_root().get_node("main")
export (NodePath) var player_path
export var grid_size = 10
export var max_visible_dist = 30
var current_player_cell_pos = Vector2()
var player

func _ready():
	player = get_node(player_path)
	if player:
		if (get_child_count() == 0):
			world_preload()
		else:
			world_update(get_player_cell_pos())

func _physics_process(delta):
	if (player):
		var cell_pos = get_player_cell_pos()
		if current_player_cell_pos != cell_pos:
			world_update(cell_pos)

func world_preload():
	var cell_num = int(pow(max_visible_dist / grid_size * 2 + 1, 2))
	for i in range(cell_num):
		add_child(preload("res://objects/WorldCells/WaterCell.tscn").instance())
	world_update(get_player_cell_pos())
	
func get_player_cell_pos():
	var grid_x = int(player.global_transform.origin.x / grid_size)
	var grid_y = int(player.global_transform.origin.z / grid_size)
	var cell_pos = Vector2(grid_x, grid_y)
	return cell_pos


func world_update(cell_pos):
	current_player_cell_pos = cell_pos
	main_node._show(str(cell_pos.x) + " " + str(cell_pos.y))
	var viz_cells = {}
	for i in range(-max_visible_dist / grid_size, max_visible_dist / grid_size + 1):
		for j in range(-max_visible_dist / grid_size, max_visible_dist / grid_size + 1):
			viz_cells[Vector2(cell_pos.x + i, cell_pos.y + j)] = ""
	var cells_to_move = []
	for o in get_children():
		var o_pos = o.currect_cell_pos
		if !viz_cells.has(o_pos):
			cells_to_move.append(o)
		else:
			viz_cells[o_pos] = o.get_path()
	if cells_to_move.size() > 0:
		var current_move_index = 0
		for k in viz_cells.keys():
			if viz_cells[k] == "" and current_move_index < cells_to_move.size():
				move_cell(cells_to_move[current_move_index], k)
				current_move_index += 1


func move_cell(obj, cell_pos):
	print(cell_pos)
	var pos = obj.global_transform.origin
	pos.x = cell_pos.x * grid_size
	pos.z = cell_pos.y * grid_size
	obj.global_transform.origin = pos
	obj.currect_cell_pos = cell_pos
