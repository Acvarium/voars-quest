extends RigidBody

var motor_enabled = false

func enable_motor(value):
	motor_enabled = value

func _physics_process(delta):
	if motor_enabled:
		apply_impulse(
			$MotorPoint.global_transform.origin - global_transform.origin,
			-$MotorPoint.global_transform.basis.x.normalized() * delta * 2.0
		)
#	if Input.is_action_pressed("ui_left"):
#		var rot = $MotorPoint.transform.basis.rotated(Basis().y, delta * 1.0)
#		$MotorPoint.transform.basis = rot
#	if Input.is_action_pressed("ui_right"):
#		var rot = $MotorPoint.transform.basis.rotated(Basis().y, -delta * 1.0)
#		$MotorPoint.transform.basis = rot


func set_dir(dir_basis):
	pass
#	$MotorPoint.global_transform.basis = dir_basis
